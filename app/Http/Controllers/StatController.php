<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\ClinicStatistic;
use App\ClinicStatisticScenario;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 12.02.19
 * Time: 1:40
 */


class StatController extends Controller
{
    private function isValidClinic($clinic) {
        $statistic = $clinic['statistic'];

        //Check to send statistic to server by producer
        $now = new \DateTime();
        $diff = date_diff($statistic['created_at'],$now);
        if (($diff->d > 0) || ($diff->h >= 12)) {
            return false;
        }
        //Check status of sent messages
        if ($statistic['number_total_messages'] > 0) {
            $sent_messages_percent = $statistic['number_ok_messages'] / $statistic['number_total_messages'];
            if ($sent_messages_percent < 0.8) {
                return false;
            }
        }

        //Check last import
        $diff = date_diff($statistic['last_import'],$now);
        if (($diff->d > 0) || ($diff->h >= 12)) {
            return false;
        }

        //Check last visit_record
        $diff = date_diff($statistic['last_visit_record'],$now);
        if (($diff->d > 0) || ($diff->h >= 12)) {
            return false;
        }

        //Check last response
        $diff = date_diff($statistic['last_response'],$now);
        if (($diff->d > 0) || ($diff->h >= 12)) {
            return false;
        }

        return true;
    }

    function move_to_top(&$array, $key) {
        $temp = array($key => $array[$key]);
        unset($array[$key]);
        $array = $temp + $array;
    }

    public function index() {
        $clinics = Clinic::all();
        foreach ($clinics as $clinic) {
            $statistic = ClinicStatistic::where('clinic_id','=',$clinic['id'])
                ->orderByDesc('created_at')
                ->first();

            $scenarios = ClinicStatisticScenario::where('clinic_statistic_id','=',$statistic['id'])
                ->get();



            $statistic['scenarios'] = $scenarios;
            $clinic['statistic'] = $statistic;

            $clinic['is_valid'] = $this->isValidClinic($clinic);
        }


        $clinics = $clinics->sortBy('is_valid');

        return view('statistic', ['clinics' => $clinics]);
    }

    private function get_error($error, $error_detail)
    {
        return response()->json([
            "error" => $error,
            "error_details" => $error_detail]);
    }

    public function receive(Request $request)
    {
        $error = 0;
        $error_detail = "";
        if (!$request->hasFile("data")) {
            return $this->get_error(1, "No attached statistic file. You need attach statistic as param \"data\"");
        }

        if (!$request->file("data")->isValid()) {
            return $this->get_error(2,"Invalid file");
        }

        $json = json_decode(file_get_contents($request["data"]), true);
        try
        {
            DB::beginTransaction();


            $clinic_id = $json['clinic_id'];
            $clinic_name = $json['clinic_name'];
            $number_ok_messages = $json['number_ok_messages'];
            $number_total_messages = $json['number_total_messages'];
            $last_database_import = $json['last_database_import'];
            $last_patient_response = $json['last_patient_response'];
            $last_patient_visit_record = $json['last_visit_record'];
            $scenarios = $json['scenarios'];

            $clinic = Clinic::firstOrCreate(['id' => $clinic_id], ['name' => $clinic_name]);
            $clinic['name'] = $clinic_name;
	    $clinic->save();

            $statistic = new ClinicStatistic();
            $statistic['clinic_id'] = $clinic_id;
            $statistic['number_ok_messages'] = $number_ok_messages;
            $statistic['number_total_messages'] = $number_total_messages;
            $statistic['last_import'] = Carbon::parse($last_database_import);
            $statistic['last_response'] = Carbon::parse($last_patient_response);
            $statistic['last_visit_record'] = Carbon::parse($last_patient_visit_record);
            $statistic->save();


            foreach ($scenarios as $scenario) {
                $scenario_name = $scenario['name'];
                $scenario_patient_count = $scenario['patients_count'];


                $database_scenario = new ClinicStatisticScenario();
                $database_scenario['clinic_statistic_id'] = $statistic['id'];
                $database_scenario['name'] = $scenario_name;
                $database_scenario['patients_count'] = $scenario_patient_count;
                $database_scenario->save();

            }

            DB::commit();

        }
        catch (Exception $e) {
            DB::rollBack();

            return $this->get_error(10, "Unknown error. ".$e->getMessage());
        }

        return response()->json([
            "error" => 0,
            "error_details" => "",
	    "json" => $json
        ]);
    }
}