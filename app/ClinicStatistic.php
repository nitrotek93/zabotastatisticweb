<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicStatistic extends Model
{
    protected $dates = ['last_import', 'last_response','last_visit_record'];
    //
}
