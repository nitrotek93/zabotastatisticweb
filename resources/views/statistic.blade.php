<!-- Stored in resources/views/layouts/app.blade.php -->

<html>
<head>

    @include('partials.head')
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
        });
    </script>
</head>

<body>

@include('partials.nav')

<div class="container">
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th scope="col">
                    ID клиники
                </th>
                <th scope="col">
                    Название клиники
                </th>
                <th scope="col">
                    Последняя отправка
                </th>
                <th scope="col">
                    Рез-ты последней отправки (отпр/статус ок)
                </th>
                <th scope="col">
                    Последний импорт
                </th>
                <th scope="col">
                    Время создания последней записи на прием
                </th>
                <th scope="col">
                    Последний пришедший ответ
                </th>
                <th scope="col">
                    Сценарии, сработавшие за 24 часа
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach ($clinics as $clinic)
            @if ($clinic->is_valid === false)
                <tr class="no-valid-clinic">
            @else
                <tr>
            @endif

                <td>
                    {{ $clinic->id }}
                </td>
                <td>
                    {{ $clinic->name }}
                </td>
                <td>
                    {{ $clinic->statistic->created_at }}
                </td>
                <td>
                    {{ $clinic->statistic->number_ok_messages }} / {{ $clinic->statistic->number_total_messages }}
                </td>
                <td>
                    {{ $clinic->statistic->last_import }}
                </td>
                <td>
                    {{ $clinic->statistic->last_visit_record }}
                </td>
                <td>
                    {{ $clinic->statistic->last_response }}
                </td>
                <td>
                    <div class="dropdown">
                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ count($clinic->statistic->scenarios) }}
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            @foreach ($clinic->statistic->scenarios as $scenario)
                                <table class="dropdown-item table-borderless">
                                    <td>
                                        {{ $scenario->name }}
                                    </td>
                                    <td>
                                        {{ $scenario->patients_count }}
                                    </td>
                                </table>
                            @endforeach
                        </div>
                    </div>


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @yield('content')
</div>

</body>
</html>